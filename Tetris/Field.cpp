#include "stdafx.h"
#include "Field.h"
#include "Utility.h"


Field::Field(){
	speed = 1;
	currentShape = new Shape(speed);
	//nextShape = new Shape();
}

Field::~Field()
{
}

void Field::addBlock(int _x, int _y, int _color){
	// find block and set it
	for (int x = 0; x < BLOCKSPERROW; x++){
		for (int y = 0; y < BLOCKSPERCOL; y++){
			//collides with other blocks
			if (fieldBlocks[x][y]->xPos == _x && fieldBlocks[x][y]->yPos == _y){
				fieldBlocks[x][y]->color = _color;
			}
		}
	}
}

void Field::addShapeToBlocks(){
	for (int x = 0; x < 4; x++){
		for (int y = 0; y < 4; y++){
			if (currentShape->blocks[x][y]->color >= 0)
				addBlock(currentShape->blocks[x][y]->xPos, currentShape->blocks[x][y]->yPos, currentShape->blocks[x][y]->color);
		}
	}
}

int Field::collidesWithField(int _x, int _y, int direction){
	//check if it collides with bottom of field first
	if (currentShape->yPos == (FIELDY)){
		return static_cast<int>(CollisionType::Down);
	}

	// then see if if it collides with other blocks in field
	for (int x = 0; x < BLOCKSPERROW; x++){
		for (int y = 0; y < BLOCKSPERCOL; y++){
			//collides with other blocks
			if (fieldBlocks[x][y]->xPos == _x && fieldBlocks[x][y]->yPos == (_y - BLOCKHEIGHT) && fieldBlocks[x][y]->color >= 0){
				return static_cast<int>(CollisionType::Down);
			}
		}
	}


	//if there are no major collisions, check for left/right collision
	
	if (direction == 2 || direction == 4){
		for (int x = 0; x < 4; x++){
			for (int y = 0; y < 4; y++){
				//collides with right wall
				if (direction == 2 && currentShape->blocks[x][y]->xPos + BLOCKWIDTH >= FIELDX + (BLOCKSPERROW * BLOCKWIDTH) && currentShape->blocks[x][y]->color >= 0){
					return static_cast<int>(CollisionType::Side);
				}
				// left wall
				else if (direction == 4 && currentShape->blocks[x][y]->xPos - BLOCKWIDTH < FIELDX && currentShape->blocks[x][y]->color >= 0){
					return static_cast<int>(CollisionType::Side);
				}

			}
		}
	}

	return static_cast<int>(CollisionType::None);
}

int Field::checkCollision(int direction){
	CollisionType collisionType;
	for (int x = 0; x < 4; x++){
		for (int y = 0; y < 4; y++){
			collisionType = static_cast<CollisionType>(collidesWithField(currentShape->blocks[x][y]->xPos, currentShape->blocks[x][y]->yPos, direction));
			if (collisionType != CollisionType::None)
				return static_cast<int>(collisionType);
		}
	}
	return 0;
}

void Field::drawBlocks(){
	for (int x = 0; x < BLOCKSPERROW; x++){
		for (int y = 0; y < BLOCKSPERCOL; y++){
			if (fieldBlocks[x][y]->color >= 0){
				fieldBlocks[x][y]->draw();
			}
		}
	}
}

void Field::loadBlocks(){
	for (int x = 0; x < BLOCKSPERROW; x++){
		for (int y = 0; y < BLOCKSPERCOL; y++){
			fieldBlocks[x][y] = new Block(FIELDX + (x * BLOCKWIDTH), FIELDY + (y * BLOCKHEIGHT));
			//fieldBlocks[x][y] = new Block();
		}
	}
}

void Field::start(){
	currentShape->start();
	loadBlocks();
	//nextShape->start();
}

void Field::shiftFieldBlocks(int _y, int rowCount){
	int emptyRowCount = 0;
	bool rowHasBlocks = false; //prove true
	for (int y = _y; y < BLOCKSPERCOL; y++)
	{
		rowHasBlocks = false; //reset
		for (int x = 0; x < BLOCKSPERROW; x++){
			if (fieldBlocks[x][y + rowCount]->color >= 0)
				rowHasBlocks = true;

			 fieldBlocks[x][y]->color = fieldBlocks[x][y + rowCount]->color;
		}
		if (rowHasBlocks == false){
			emptyRowCount++;
			if (emptyRowCount > rowCount)
				return;
		}
	}
}

void Field::clearLines(int (&lines)[4]){
	if (lines[0] >= 0 && lines[1] >= 0 && lines[2] >= 0 && lines[3] >= 0) { //a tetris!
		shiftFieldBlocks(lines[0], 4);
	}
	else if (lines[0] >= 0 && lines[1] >= 0  && lines[2] >= 0) { //a tre!
		shiftFieldBlocks(lines[0], 3);
	}
	else if (lines[0] >= 0 && lines[1] >= 0 && lines[2] < 0 && lines[3] < 0) { //a deucy!
		shiftFieldBlocks(lines[0], 2);
	}
	else if (lines[0] >= 0 && lines[1] < 0 && lines[2] < 0 && lines[3] >= 0) { //a 1-4!
		shiftFieldBlocks(lines[0], 1);
		shiftFieldBlocks(lines[3] -1, 1);
	}
	else if (lines[0] >= 0 && lines[1] < 0 && lines[2] >= 0 && lines[3] < 0) { //a 1-3!
		shiftFieldBlocks(lines[0], 1);
		shiftFieldBlocks(lines[2] - 1, 1);
	}
	else if (lines[0] >= 0 && lines[1] < 0 && lines[2] < 0 && lines[3] < 0) { //a UNO!
		shiftFieldBlocks(lines[0], 1);
	}

}

void Field::checkLines(){
	int lines[] = { -1, -1, -1, -1 };
	bool prevLineHasBlocks = true; //default to true
	bool isLine = false;
	bool foundLine = false;

	for (int y = 0; y < BLOCKSPERCOL;y++){
		if (prevLineHasBlocks == false)
			break;

		prevLineHasBlocks = false; // set to false and prove true
		isLine = true; //set true and prove false
		for (int x = 0; x < BLOCKSPERROW; x++){
			if (fieldBlocks[x][y]->color == -1)
				isLine = false;
			else
				prevLineHasBlocks = true;
		}
		if (isLine)
		{
			if (foundLine == false){
				foundLine = true;
				lines[0] = y;
			}
			else{
				// fill in appriate spot in array
				lines[y - lines[0]] = y;
			}
		}
			
	}
	if (foundLine)
		clearLines(lines);
}

void Field::update(int direction){
	CollisionType collisionType = static_cast < CollisionType > (checkCollision(direction));
	if (collisionType == CollisionType::Down){
		addShapeToBlocks();
		checkLines();
		currentShape = new Shape(speed);
	}
	else if (collisionType != CollisionType::Side){
		currentShape->update(direction);
	}
}

void Field::draw(){
	Utility::drawRect(FIELDX, FIELDY, BLOCKWIDTH * BLOCKSPERROW, BLOCKHEIGHT * BLOCKSPERCOL, 1);
	currentShape->draw();
	drawBlocks();
}

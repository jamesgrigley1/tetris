#pragma once
class Block
{

public:
	int xPos;
	int yPos;
	int color; //1=purple, 2=red, 3=green, 4=blue, 5=orange, 6=teal

	Block();
	Block(int _x, int _y);
	Block(int _color);
	~Block();

	void setPosition(int x, int y);
	void destroy();

	void start();
	void update();
	void draw();

};

